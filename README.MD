Simple RESTful API training.
This project is Port terminal management program.
All control is done by API calls.


## Table of Contents
1. [Installation](#installation)
2. [Overview](#overview)
3. [Testing](#third-example)
4. [Usage](#usage)
5. [To Do](#to-do)



## Installation
`git clone https://gitlab.com/predtech1988/port_terminal.git`
`cd port_terminal`
open env_template.txt `nano env_template.txt`
set POSTGRES_PASSWORD and FLASK_SECRET_KEY as you like
rename this file to '.env' -> `mv env_template.txt .env` [Linux terminal or Windows Power Shell]
(in Linux sudo before docker....)
`docker-compose build`
`docker-compose up -d`
## Overview
We can
    - Add new empty container in our port.
    - Add new container with boxes (items)
    - Remove containers and boxes (send on loading on the ship)
    - View boxes in the container and nested containers inside boxes
    - Boxes can contain smaller container inside

Container and Box objects consists of:

    id: unique integer
    name: unique string
    type: char (C - container, B - box)
    parent: id of parent (container or box)
    children: list of id's (containers and boxes)

Implemented:  
 - CRUD
    - C - Create A Record `add_item()`
    - R - Read A Particular Record By Query `get_item_by_filter("id", 5)`
    - U - Update A Particular Record `update_item_by_id()`
    - D - Delete A Particular Record By Id `delete_item_by_id()`
 - Registration, Login, access to protected route when logged in
 - Added simple testing api_test.py
 - If id = 0 or not specified - all objects in the database should be listed
 - If the given object has children - display all of the children objects, and the object itself
 - If the object does not exist return 404
 - If ID not a number return code 400
 - Added response format "XML" 

## Testing
open in browser to test http://localhost/s  ['You are in!']
to run simple testing we need install requests lib
`docker container exec -it backend pip install requests`
`docker container exec -it backend python api_test.py`
`docker container exec -it backend python few_more_tes.py`


## Usage

### Register:
```
good_user_data = {
    "username": "Alex60",
    "email": "Alex60@m.com",
    "password": "123456",
}
def registration(user):
    endpoint = "http://localhost/register"
    headers = {"content-type": "application/json"}
    resp = requests.post(url=endpoint, json=user, headers=headers)
    return resp

registration(good_user_data)
```

### Login:
Receive token
```
def login(user):
    endpoint = "http://localhost/login"
    data = {"email": user["email"], "password": user["password"]}
    resp = requests.post(url=endpoint, json=data)
    token = resp.json()["info"][0]["access_token"]
    return token

token = login(user=good_user_data)
```
## To Do
    [] - Logger
    [] - Send email with code for registration confirmation (is_active field in db)
    [] - Separate DB. users -> postgres, containers -> Mongo
    [] - Add caching
