import json

import requests

data_5 = {"name": "Only name field"}

data_6 = {}
data_7 = ["name", "Box_7"]
data_8 = {
    "name": "Super_shine_box",
    "obj_type": "B",
    "parent": None,
    "children": None,
}

good_user_data = {
    "email": "Alex48@m.com",
    "password": "123456",
}
good_user = {
    "email": "Alex50@m.com",
    "password": "123456",
}


def login(user):
    endpoint = "http://localhost:5000/login"
    data = {"email": user["email"], "password": user["password"]}
    resp = requests.post(url=endpoint, json=data)
    assert resp.status_code == 200
    token = resp.json()["info"][0]["access_token"]
    return token


my_token = login(user=good_user_data)
print(my_token)


def get_items(data, token):
    endpoint = "http://localhost:5000/api/v1.0/object/"
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + token,
    }
    resp = requests.get(url=endpoint, json=data, headers=headers)
    resp_json = resp.json()
    print(json.dumps(resp_json, indent=4))
    return resp_json


def add_items(data, token):
    endpoint = "http://localhost:5000/api/v1.0/object/add"
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + token,
    }
    resp = requests.post(url=endpoint, json=data, headers=headers)
    return resp.content


def update_items(id, data, token):
    endpoint = f"http://localhost:5000/api/v1.0/object/update/{id}"
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + token,
    }
    params = {"key": data[0], "val": data[1]}
    resp = requests.post(url=endpoint, params=params, headers=headers)
    return resp.content


def del_container(id, token):
    endpoint = f"http://localhost:5000/api/v1.0/object/delete/{id}"
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + token,
    }
    resp = requests.post(url=endpoint, headers=headers)
    print(resp.status_code)
    return resp.text


print("##############################")
print("######   ADD NEW ITEM  'new object must have 4 fields' ######")
print(add_items(data_5, my_token))
print("##############################")

print("##############################")
print("######   ADD NEW ITEM   ######")
print(add_items(data_8, my_token))
print("##############################")

print("####     Update item(user without rights     #######")
print(update_items(15, data_7, login(user=good_user)))
print("############################################")

print("Item id 15 before updating")
print(get_items({"id": 15}, my_token))

print("#### Update item (user 'operator'####")
print(update_items(15, data_7, my_token))
print("#####################################")

print("Item id 13 AFTER updating")
print(get_items({"id": 15}, my_token))

print("Item id 9 before updating (children have parent id 9")
print(get_items({"id": 9}, my_token))
print("Item id 3 before updating (parent id 9")
print(get_items({"id": 3}, my_token))

print("########Delete container#############")
print(del_container(9, my_token))
print("####################################")

print("Item id 9 AFTER updating")
print(get_items({"id": 9}, my_token))
print("Item id 3 AFTER updating id 9 removed (parent id None")
print(get_items({"id": 3}, my_token))
print("$$$$$$$$$$$$$$$$$$$$$")
print("OK")
print("$$$$$$$$$$$$$$$$$$$$$")
