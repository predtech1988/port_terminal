from datetime import timedelta
from typing import Any
from uuid import uuid1

from flask_jwt_extended import create_access_token
from passlib.hash import bcrypt
from sqlalchemy import Column, Integer, Boolean
from sqlalchemy.dialects import postgresql
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Container(Base):
    __tablename__ = "objects"
    id = Column(postgresql.BIGINT, autoincrement=True, primary_key=True)
    name = Column(postgresql.VARCHAR(50), nullable=False)
    obj_type = Column(postgresql.CHAR(1), nullable=False)
    parent = Column(postgresql.BIGINT, nullable=True)
    children = Column(postgresql.ARRAY(Integer, dimensions=1), nullable=True)

    def __init__(self, **kwargs):
        self.name = kwargs.get("name")
        self.obj_type = kwargs.get("obj_type")
        self.parent = kwargs.get("parent")
        self.children = kwargs.get("children")
        msg = {"Ok": f"Container: '{self.name}' successfully created."}
        print(msg)

    def __repr__(self) -> str:
        return f"<Container(id= {self.id},name = {self.name}, obj_type = {self.obj_type},\
        parent = {self.parent}, children = {self.children})"

    def to_dict(self) -> dict[str, Any]:
        """
        Takes Container object and convert it to the Python Dictionary
        """
        return {
            "id": self.id,
            "name": self.name,
            "obj_type": self.obj_type,
            "parent": self.parent,
            "children": self.children,
        }


class User(Base):
    __tablename__ = "users"
    id = Column(postgresql.BIGINT, autoincrement=True, primary_key=True)
    uid = Column(postgresql.VARCHAR(50), unique=True, nullable=False)
    username = Column(postgresql.VARCHAR(50), unique=True, nullable=False)
    email = Column(postgresql.VARCHAR(50), unique=True, nullable=False)
    hashed_password = Column(postgresql.VARCHAR(100))
    roles = Column(
        postgresql.ARRAY(postgresql.VARCHAR(100), dimensions=1), nullable=True
    )
    is_active = Column(Boolean, default=True, server_default="true")

    def __init__(self, **kwargs):
        self.uid = uuid1()
        self.username = kwargs.get("username")
        self.email = kwargs.get("email")
        self.hashed_password = bcrypt.hash(kwargs.get("password"))
        self.roles = ["user"]
        self.is_active = False

    def get_token(self, expire_time=24):
        expire_delta = timedelta(expire_time)
        token = create_access_token(identity=self.uid, expires_delta=expire_delta)
        return token

    @classmethod
    def authenticate(cls, email, password):
        user = cls.query.filter(cls.email == email).one()
        if not bcrypt.verify(password, user.hashed_password):
            return None
        return user
