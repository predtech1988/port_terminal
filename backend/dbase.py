import json
from typing import Any, Dict, List, Optional
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from models import Base, Container, User

import config


DB_DRIV = config.DB_DRIVER
DB_USER = config.DB_USER
DB_PASS = config.DB_PASS
DB_NAME = config.DB_NAME
DB_HOST = config.DB_HOST
DB_PORT = config.DB_PORT
DATABASE_URI = f"{DB_DRIV}://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

engine = create_engine(DATABASE_URI, echo=False)
session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base.query = session.query_property()


def drop_all_tables():
    Base.metadata.drop_all(engine)


def create_all_tables():
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)


def from_dump_to_db():
    """
    Load's json dump with dictionaries, converting them in the Container object's\n
    and then add to the DB.
    """
    with open("dump.json", "r") as f:
        data = json.load(f)
        for dic in data:
            add_item(dic)
    msg = {"Ok": "All dump successfully exported to the DB."}
    print(msg)
    return


######     CRUD  functions for the 'objects' table    ######


def add_item(item: Dict) -> dict[str, str]:
    new_item = Container(**item)
    session.add(new_item)
    session.commit()
    session.close()
    msg = {"Ok": f"Item: '{item['name']}' successfully added to the DB."}
    print(msg)
    return msg


def get_all_items() -> List:
    all_items = session.query(Container).all()
    session.close()
    msg = {"Ok": "All items successfully extracted from the DB."}
    print(msg)
    return [item.to_dict() for item in all_items]


def get_children(lst: list) -> list:
    return [Container.query.filter_by(id=child).first().to_dict() for child in lst]


def get_filtered_item(id: int, filter_: str) -> list:
    params = {filter_[0]: filter_[1]}
    root_item = Container.query.filter_by(id=id).first().to_dict()
    child_ids = root_item["children"]
    filtered_items = [root_item]
    for child in child_ids:
        tmp_item = Container.query.filter_by(id=child).filter_by(**params).first()
        if tmp_item:
            filtered_items.append(tmp_item.to_dict())
    return filtered_items


def get_item_by(filter: str, val: Any, all: bool = False) -> Dict:
    """
    Searching item by given filter and returns Dictionary object\n
    :Parameters:
      - `filter` (`string`) - colum to filter "id", "name", etc
      - `val` (`Any`) - value for filter `int` for "id", `str` for name, etc\n
      - `all` (`bool`) - return filtered `list` of dictionaries\n
    :return: (`dictionary`) Dictionary object with filtered item.

    Examples:
        get_item_by("id", 5)
            -> {'id': 5, 'name': 'Box_Y', 'obj_type': 'B', 'parent': 6, 'children': None}
        get_item_by("obj_type", "C", `all=True`)
            -> [{'id': 1, 'name': ...}...{'id': 11, 'name': 'Container_4',...}]
        get_item_by"id", 50, all=True)
            -> {'Error': "Item: {'id': 50} does not exists!"}
    """
    params = {filter: val}

    try:
        getattr(Container, filter)
    except:
        msg = {"code": 401, "Error": f"Item: {params} does not exists!"}
        print(msg)
        return msg
    if filter == "id":
        try:
            int(params["id"])
        except:
            msg = {"code": 400, "Error": f"ID must be number! Params: {params}"}
            print(msg)
            return msg

    if all:
        containers_list = Container.query.filter_by(**params).all()
        if not containers_list:
            msg = {"code": 404, "Error": f"Item: {params} does not exists!"}
            print(msg)
            return
        items_list = [cont.to_dict() for cont in containers_list]
        msg = {"Ok": "All filtered items successfully extracted from the DB."}
        print(msg)
        return items_list
    item = Container.query.filter_by(**params).first()
    if item:
        msg = {"Ok": "Item successfully extracted from the DB."}
        print(msg)
        item = item.to_dict()
        if item.get("children"):
            oyakodon = get_children(item["children"])
            oyakodon.append(item)
            return oyakodon
        return item
    msg = {"code": 404, "Error": f"Item: {params} does not exists!"}
    print(msg)
    return msg


def update_item_by_id(id: int, key: str, new_val: Any) -> None:
    """
    Updating item by given ID\n
    :Parameters:
      - `id` (`int`) - item's ID
      - `key` (`str`) - field to update\n
      - `new_val` (`any`) - new value for that filed\n
    :return: (`None`)

    Examples:
        update_item_by_id(12, "name", "File_55")\n
        update_item_by_id(5, "children", [2, 5])
    """
    obj = Container.query.filter_by(id=id).first()
    if not obj:
        msg = [{"code": 404}, {"Error": f"No item with id: {id} found!"}]
        print(msg)
        return msg
    if hasattr(obj, key):
        setattr(obj, key, new_val)
        session.add(obj)
        session.commit()
        session.close()
        msg = [{"code": 200}, {"Ok": "Item successfully updated."}]
        print(msg)
        return msg
    msg = [
        {"code": 500},
        {"Error": f"Something went wrong while updating item id: {id}, attr: {key}"},
    ]
    print(msg)
    return msg


def delete_item_by_id(id: int) -> None:
    Container.query.filter_by(id=id).delete()
    session.commit()
    session.close()
    msg = [{"code": 200}, {"Ok": f"Item id: {id} successfully deleted"}]
    print(msg)
    return msg


def clear_parent(id, to_del):
    parent_obj = Container.query.filter_by(id=id).first()
    updated = [child for child in getattr(parent_obj, "children") if child != to_del]
    setattr(parent_obj, "children", updated)
    session.add(parent_obj)
    session.commit()
    session.close()
    return


def del_and_clean(id: int):
    item = Container.query.filter_by(id=id).first()
    if not item:
        msg = [
            {
                "code": 404,
            },
            {"Error": f"No item with id: {id} found!"},
        ]
        return msg
    children = getattr(item, "children")
    parent = getattr(item, "parent")
    if parent:
        # if item have parent, we remove id of deleting item
        clear_parent(parent, id)

    if children:
        for child in children:
            update_item_by_id(child, "parent", None)
    try:

        return delete_item_by_id(id)
    except:
        return [{"code": 500}, {"Error": f"While removing item id: {id} !"}]


#### DB TESTING ########
def initiate_db():
    # For dev purposes. In final version, db will be
    # already in Docker container
    drop_all_tables()
    create_all_tables()
    from_dump_to_db()


def test_db():
    print(get_item_by("name", "Box_1"))
    # {'id': 2, 'name': 'Box_1', 'obj_type': 'B', 'parent': 1, 'children': None}

    new_item = {
        "id": 13,
        "name": "Box_New",
        "obj_type": "B",
        "parent": None,
        "children": None,
    }

    add_item(new_item)
    print(get_item_by("id", 13))
    # {'id': 13, 'name': 'Box_New', 'obj_type': 'B', 'parent': None, 'children': None}
    update_item_by_id(13, "name", "BBW")
    print(get_item_by("id", 13))
    # {'id': 13, 'name': 'BBW', 'obj_type': 'B', 'parent': None, 'children': None}
    delete_item_by_id(13)
    # {'Ok': 'Item id: 13 successfully deleted'}
    print(get_item_by("id", 13))
    # {'Error': "Item: {'id': 13} does not exists!"}


# test_db()
# create_all_tables()

# TODO separate help text from source code
help_text: list
with open("help.json", "r") as f:
    help_text = json.load(f)

######     CRUD  functions for the 'users' table    ######
def add_new_user(data):
    if not (bool(data["username"]) and bool(data["email"]) and bool(data["password"])):
        msg = [
            {"code": 400},
            {"error": "One of the fields (username, password, email) is empty!"},
        ]
        print(msg)
        return msg
    if User.query.filter_by(username=data["username"]).first():
        msg = [
            {"code": 400},
            {"error": "User with this credentials already exists!"},
        ]
        print(msg)
        return msg
    if User.query.filter_by(email=data["email"]).first():
        msg = [
            {"code": 400},
            {"error": "Email already in use!"},
        ]
        print(msg)
        return msg
    try:
        new_user = User(**data)
        session.add(new_user)
        session.commit()
        token = new_user.get_token()
        msg = [
            {"code": 200},
            {
                "info": [
                    {"access_token": token},
                    {
                        "help": help_text,
                    },
                ]
            },
        ]
        print(msg[1]["info"][1])
        session.close()
        return msg
    except:
        msg = [
            {"code": 500},
            {"error": "Something went wrong, while adding new user!"},
        ]
    print(msg)
    return msg


def login_user(kwargs):
    user = User.query.filter_by(email=kwargs["email"]).first()
    if not user:
        msg = [{"code": 400}, {"error": "User not found!"}]
        print(msg)
        return msg
    logged_in_user = User.authenticate(kwargs["email"], kwargs["password"])
    if not logged_in_user:
        msg = [{"code": 400}, {"error": "No user with this password!"}]
        print(msg)
        return msg
    msg = [
        {"code": 200},
        {
            "info": [
                {"access_token": logged_in_user.get_token()},
                {
                    "help": help_text,
                },
            ]
        },
    ]
    print(msg[1]["info"][1])
    return msg


def check_user_rights(user_uid):
    user = User.query.filter_by(uid=user_uid).first()
    if not user:
        msg = [{"code": 404}, {"error": "User not found, Try Login again."}]
        print(msg)
        return msg
    if "operator" in user.roles:
        return [{"code": 200}]
    msg = [{"code": 403}, {"error": "User don't have enough rights!"}]
    print(msg)
    return msg
