from flask import jsonify, request, url_for, redirect, make_response
from app import app

from sqlalchemy.sql.operators import is_
from dbase import add_new_user, login_user
from flask_jwt_extended import jwt_required, get_jwt_identity
from dbase import (
    get_item_by,
    get_all_items,
    get_filtered_item,
    add_item,
    check_user_rights,
    update_item_by_id,
    del_and_clean,
)
from dicttoxml import dicttoxml

#### Some functions ####
def is_int(id):
    try:
        int(id)
        return id
    except:
        return False


####    Routes  ####
@app.route("/")
def main_page():
    return redirect("/login", code=302)


@app.route("/login", methods=["POST"])
def login_page():
    resp = login_user(request.json)
    return make_response(jsonify(resp[1]), resp[0]["code"])


@app.route("/register", methods=["POST"])
def register_page():
    resp = add_new_user(request.json)
    return make_response(jsonify(resp[1]), resp[0]["code"])


@app.route("/secret")
@jwt_required()
def secret_place():
    return "<h1>You are in!"


@app.route("/s", methods=["GET"])
def se():
    print(request.data)
    print(bool(request.args))
    return "<h1>You are in!"


@app.route("/api/v1.0/object/", methods=["GET"])
@jwt_required()
def get_obj():
    id = request.get_json().get("id") if (request.get_json().get("id")) else 0
    filter_ = (
        request.get_json().get("filter") if (request.get_json().get("filter")) else None
    )
    format_ = (
        request.get_json().get("format")
        if (request.get_json().get("format"))
        else "json"
    )
    if id == 0 and filter_:
        # If we have ID == 0 and Some filter
        all_items = get_item_by(filter_[0], filter_[1], all=False)
        if not all_items:
            msg = {"Error": f"No items found!"}
            print(msg)
            return jsonify({"code": 404}, msg)
        if format_ == "xml":
            return app.response_class(
                dicttoxml(all_items, attr_type=False), mimetype="application/xml"
            )
        return jsonify({"code": 200}, all_items)
    if id == 0 and not filter_:
        # If we have ID == 0 and None filter
        all_items = get_all_items()
        if all_items and format_ == "json":
            return jsonify({"code": 200}, all_items)
        if all_items and format_ == "xml":
            return app.response_class(
                dicttoxml(all_items, attr_type=False), mimetype="application/xml"
            )
        msg = {"Error": f"No items found!"}
        print(msg)
        return jsonify({"code": 404}, msg)
    if id and not filter_:
        # If we have certain ID and no filter
        all_items = get_item_by("id", id)
        if type(all_items) == list:
            code = 200
        else:
            code = all_items.get("code") if all_items.get("code") else 200
        if format_ == "xml":
            return app.response_class(
                dicttoxml(all_items, attr_type=False), mimetype="application/xml"
            )
        return jsonify({"code": code}, all_items)
    if filter_[0] in ["id", "name", "obj_type", "parent"]:
        # If we have certain ID and some filter
        all_items = get_filtered_item(id, filter_)
        if format_ == "xml":
            return app.response_class(
                dicttoxml(all_items, attr_type=False), mimetype="application/xml"
            )
        return jsonify({"code": 200}, all_items)

    return {
        "code": 500,
        "Error": "Unknown error, something went wrong while working with DB",
    }


@app.route("/api/v1.0/object/add", methods=["POST"])
@jwt_required()
def add_new_container():
    current_user = check_user_rights(get_jwt_identity())
    if current_user[0]["code"] == 200:
        item = request.get_json()
        if not len(item.keys()) == 4:
            return jsonify(
                {"code": 400},
                {
                    "Error": "new object must have 4 fields: 'name', 'obj_type', 'parent', 'children'"
                },
            )
        new_item = {
            "name": item.get("name"),
            "obj_type": item.get("obj_type"),
            "parent": item.get("parent"),
            "children": item.get("children"),
        }
        msg = add_item(new_item)
        return jsonify({"code": 200}, msg)
    return jsonify(current_user)


@app.route("/api/v1.0/object/update/<id>", methods=["POST"])
@jwt_required()
def update_container(id):
    current_user = check_user_rights(get_jwt_identity())
    if not is_int(id):
        return make_response(
            jsonify({"Error": "Item ID must be Integer"}), 400
        )
    if current_user[0]["code"] == 200:
        key = request.args.get("key")
        val = request.args.get("val")
        resp = update_item_by_id(int(id), key=key, new_val=val)
        return make_response(jsonify(resp[1]), resp[0]["code"])
    return jsonify(current_user)


@app.route("/api/v1.0/object/delete/<id>", methods=["POST"])
@jwt_required()
def del_container(id):
    current_user = check_user_rights(get_jwt_identity())
    if not is_int(id):
        return make_response(
        jsonify({"Error": "Item ID must be Integer"}), 400
    )
    if current_user[0]["code"] == 200:
        # check it there obj with ID
        # check is there children, if true, clear their parent, and child
        resp = del_and_clean(int(id))
        return make_response(jsonify(resp[1]), resp[0]["code"])
    return jsonify(current_user)