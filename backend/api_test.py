# import responses
import json

import requests

# run test_all()

good_user_data = {
    "username": "Alex48",
    "email": "Alex48@m.com",
    "password": "123456",
}
# Same email field
bad_user_data_1 = {
    "username": "Alex49",
    "email": "Alex48@m.com",
    "password": "123456",
}
# Same username field
bad_user_data_2 = {
    "username": "Alex48",
    "email": "Alex49@m.com",
    "password": "123456",
}
# Empty field
bad_user_data_3 = {
    "username": "",
    "email": "Alex50@m.com",
    "password": "123456",
}

# get_items() Data
# If id = 0 or not specified - all objects in the database should be listed.
data_1 = {}

# If the given object has children - display all of the children objects, and the object itself
# TODO add recursion call, and show children of the children objects
data_2 ={"id":11}

# Object without children objects
data_3 = {"id":10}

# If the object does not exist return 404
data_4 = {"id":9999}

# If ID not a number return code 400
data_5 = {"id":"some_text_value"}

# Certain Id with filtered child's, only box
data_6 = {
    "id": 11,
    "filter":["obj_type", "B"],
    "format": None
}

def get_items(data, token):
    endpoint = "http://localhost/api/v1.0/object/"
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + token,
    }
    resp = requests.get(url=endpoint, json=data, headers=headers)
    resp_json = resp.json() 
    print(json.dumps(resp_json, indent=4))
    return resp_json


def go_to_secret_place(user, token="None"):
    endpoint = "http://localhost/secret"
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + token,
    }
    resp = requests.get(url=endpoint, headers=headers)
    return resp


def login(user):
    endpoint = "http://localhost/login"
    data = {"email": user["email"], "password": user["password"]}
    resp = requests.post(url=endpoint, json=data)
    assert resp.status_code == 200
    token = resp.json()["info"][0]["access_token"]
    return token


def registration(user):
    endpoint = "http://localhost/register"
    headers = {"content-type": "application/json"}
    resp = requests.post(url=endpoint, json=user, headers=headers)
    return resp


def test_all():
    # t0 = registration(good_user_data)
    # assert t0.status_code == 200
    # assert len(t0.json()["info"][0]["access_token"]) > 100
    t1 = registration(bad_user_data_1)
    assert t1.status_code == 400
    assert t1.json()["error"] == "Email already in use!"
    t2 = registration(bad_user_data_2)
    assert t2.status_code == 400
    assert t2.json()["error"] == "User with this credentials already exists!"
    t3 = registration(bad_user_data_3)
    assert t3.status_code == 400
    assert (
        t3.json()["error"] == "One of the fields (username, password, email) is empty!"
    )
    token = login(user=good_user_data)
    assert len(token) > 100
    t4 = go_to_secret_place(good_user_data)
    assert t4.status_code != 200
    t5 = go_to_secret_place(good_user_data, token=token)
    assert t5.status_code == 200
    t6 = get_items(data_1, token)
    assert t6[0]["code"] == 200
    t7 = get_items(data_2, token)
    assert t7[0]["code"] == 200
    t8 = get_items(data_3, token)
    assert t8[0]["code"] == 200
    t9 = get_items(data_4, token)
    assert t9[0]["code"] == 404
    t9 = get_items(data_5, token)
    assert t9[0]["code"] == 400
    t9 = get_items(data_6, token)
    assert t9[0]["code"] == 200
    print("ALL test's passed")
    my_token = token
    print(my_token)

if __name__ == "__main__":
    test_all()
