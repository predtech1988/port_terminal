from flask import Flask
from flask_jwt_extended import JWTManager
from config import FLASK_SECRET_KEY

app = Flask(__name__)
app.config["SECRET_KEY"] = FLASK_SECRET_KEY
jwt = JWTManager(app)

from routes import *

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
